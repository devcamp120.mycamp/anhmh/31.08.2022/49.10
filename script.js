"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gDataOrders = []; //Khai báo biến mảng (biến toàn cục) chứa thông tin dữ liệu đơn hàng
    const gORDER = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    //Biến toàn cục để lưu trữ Id & orderId đang được update
    var gOrderId;
    var gId;
    //Danh sách các Mã Giảm Giá
    var gDiscountVouchers;
    //Danh sách Pizza Size
    var gPizzaSize = ["S", "M", "L"];
    //Danh sách trạng thái 
    var gOrderStatus = [
        {
            text: "Open",
            value: "open"
        },
        {
            text: "Cancel",
            value: "cancel"
        },
        {
            text: "Confirmed",
            value: "confirmed"
        },
    ];
    //Khai báo các cột của Datatable
    const gCOLUMN_ORDER_ID = 0;
    const gCOLUMN_KICH_CO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_ID_LOAI_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4;
    const gCOLUMN_HO_TEN = 5;
    const gCOLUMN_SO_DIEN_THOAI = 6;
    const gCOLUMN_TRANG_THAI = 7;
    const gCOLUMN_ACTION = 8;
    //Định nghĩa table - chưa có data
    var gOrderTable = $("#table-orders").DataTable({
        "columns": [
            { "data": gORDER[gCOLUMN_ORDER_ID] },
            { "data": gORDER[gCOLUMN_KICH_CO] },
            { "data": gORDER[gCOLUMN_LOAI_PIZZA] },
            { "data": gORDER[gCOLUMN_ID_LOAI_NUOC_UONG] },
            { "data": gORDER[gCOLUMN_THANH_TIEN] },
            { "data": gORDER[gCOLUMN_HO_TEN] },
            { "data": gORDER[gCOLUMN_SO_DIEN_THOAI] },
            { "data": gORDER[gCOLUMN_TRANG_THAI] },
            { "data": gORDER[gCOLUMN_ACTION] },
        ],
        //Ghi đè nội dung của cột action, chuyển thành 02 button "Sửa" "Xoá"
        "columnDefs": [
            {
                "targets": gCOLUMN_ACTION,
                "className": "text-center",
                "defaultContent": `
                <div class="row">
                <div class = "col-md-6">
                    <button class="btn btn-primary btn-edit"> <i class="far fa-edit"></i></button>
                </div>
                <div class = "col-md-6">
                    <button class="btn btn-danger btn-delete"> <i class="far fa-trash-alt"></i></button>
                </div>
            </div>
            `
            }
        ]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //Hàm chạy khi trang được load
    onPageLoading();
    getDrinkListAjaxClick();
    getPizzaSizeList(gPizzaSize);
    getOrderStatusList(gOrderStatus);
    //Gán sự kiện cho nút Lọc Dữ Liệu
    $("#btn-filter-data").on("click", function () {
        onBtnFilterClick();
    });
    //Gắn event handler (sự kiện) cho nút 'Thêm'
    //Gán sự kiện nút 'Thêm' đơn hàng
    $("#btn-add-order").on("click", function () {
        onBtnAddOrderClick();
    });
    //C - Gán sự kiện khi ấn nút 'Thêm mới' trên Modal 'Thêm'
    $("#btn-create-order").on("click", function () {
        onBtnCreateOrderClick();
    });
    //U - Gán sự kiện Edit - Sửa thông tin Order
    $("#table-orders").on("click", ".btn-edit", function () {
        onBtnEditOrderClick(this);
    });
    //Gán sự kiện khi ấn nút 'Cập nhật' trên Modal Edit
    $("#btn-edit-order").on("click", function () {
        onBtnConfirmEditClick();
    });
    //D - Gán sự kiện Delete - Xoá thông tin Order
    $("#table-orders").on("click", ".btn-delete", function () {
        onBtnDeleteOrderClick(this);
    });
    //Gán sự kiện khi ấn nút 'Xác nhận' trên Modal Delete
    $("#btn-confirm-delete-order").on("click", function () {
        onBtnConfirmDeleteClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm gọi API để load data vào trang
    function onPageLoading() {
        "use strict"
        //Lấy data từ server
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                gDataOrders = responseObject;
                console.log(gDataOrders);
                loadDataToTable(gDataOrders);
            },
            error: function (error) {
                console.assert(error.responseText);
            }
        });
    }
    //Hàm xử lý khi ấn nút 'Thêm'
    function onBtnAddOrderClick() {
        "use strict";
        $("#modal-order-detail").modal("show");
        $('.modal-body').on('change', '#select-pizza-size', function () {
            var vSelectSize = $("#select-pizza-size").val();
            var vSelectDuongKinh = $('#select-duong-kinh');
            var vSelectSuon = $('#select-suon-quantity');
            var vSelectSalad = $('#select-salad-size');
            var vSelectDrinkQuant = $('#select-drink-quantity');
            var vSelectPrice = $('#select-price');
            var vSelectOrderStatus = $('#select-order-status');
            if (vSelectSize === "NOT_SELECT_SIZE") {
                $('#select-duong-kinh').val("NOT_SELECT_PIZZA_SIZE");
                $('#select-suon-quantity').val("NOT_SELECT_SUON");
                $('#select-salad-size').val("NOT_SELECT_SALAD");
                $('#select-drink-quantity').val("NOT_SELECT_DRINK_QUANT");
                $('#select-price').val("NOT_SELECT_PRICE");
            }
            if (vSelectSize === "S") {
                vSelectDuongKinh.attr('disabled', true);
                $('#select-duong-kinh').val("20");
                vSelectSuon.attr('disabled', true);
                $('#select-suon-quantity').val("2");
                vSelectSalad.attr('disabled', true);
                $('#select-salad-size').val("200");
                vSelectDrinkQuant.attr('disabled', true);
                $('#select-drink-quantity').val("2");
                vSelectPrice.attr('disabled', true);
                $('#select-price').val("150000");
                vSelectOrderStatus.attr('disabled', true);
                $('#select-order-status').val("Open");
            }
            if (vSelectSize === "M") {
                vSelectDuongKinh.attr('disabled', true);
                $('#select-duong-kinh').val("25");
                vSelectSuon.attr('disabled', true);
                $('#select-suon-quantity').val("4");
                vSelectSalad.attr('disabled', true);
                $('#select-salad-size').val("300");
                vSelectDrinkQuant.attr('disabled', true);
                $('#select-drink-quantity').val("3");
                vSelectPrice.attr('disabled', true);
                $('#select-price').val("200000");
                vSelectOrderStatus.attr('disabled', true);
                $('#select-order-status').val("Open");
            }
            if (vSelectSize === "L") {
                vSelectDuongKinh.attr('disabled', true);
                $('#select-duong-kinh').val("30");
                vSelectSuon.attr('disabled', true);
                $('#select-suon-quantity').val("6");
                vSelectSalad.attr('disabled', true);
                $('#select-salad-size').val("500");
                vSelectDrinkQuant.attr('disabled', true);
                $('#select-drink-quantity').val("4");
                vSelectPrice.attr('disabled', true);
                $('#select-price').val("250000");
                vSelectOrderStatus.attr('disabled', true);
                $('#select-order-status').val("Open");
            }
        });
    }
    //Hàm xử lý khi ấn nút 'Thêm mới' trên Modal 'Thêm'
    function onBtnCreateOrderClick() {
        "use strict";
        // Bước 0: Khai báo đối tượng chứa Order Data Object
        var vOrderObj = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVoucher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",
            email: "",
            trangThai: ""
        }
        //Bước 1: Thu thập dữ liệu
        getCreateOrderData(vOrderObj);
        //Bước 2: Validate dữ liệu
        var vIsOrderDataValidate = validateOrderCreateData(vOrderObj);
        if (vIsOrderDataValidate) {
            //Bước 3: Insert Order Data 
            postOrderApi(vOrderObj);
            //Bước 4: Xử lý Front-end
            //Gọi API lấy danh sách đơn hàng
            getAllOrder();
            //Load lại vào bảng (table)
            loadDataToTable(gDataOrders);
            //Tắt (ẩn) modal from Update User
            $("#modal-order-detail").modal("hide");
            //Xoá trắng dữ liệu trên modal
        }
    }
    //Hàm xử lý sự kiện Filter Data
    function onBtnFilterClick() {
        "use strict";
        var vPizzaType = $("#pizza-Type-Select").val();
        var vOrderStatus = $("#order-Status-Select").val();
        var vDataFilter = gDataOrders.filter(function (paramOrder, index) {
            return (vPizzaType === "All" || (paramOrder.loaiPizza != null && vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase()))
                && (vOrderStatus === "All" || (paramOrder.trangThai != null && vOrderStatus.toUpperCase() === paramOrder.trangThai.toUpperCase()))
        });
        //Gọi hàm load (đổ) dữ liệu vào table
        loadDataToTable(vDataFilter);
    }
    //Hàm gọi API Select Drink
    function getDrinkListAjaxClick() {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/drinks',
            dataType: 'json',
            type: 'GET',
            success: function (res) {
                handleDrinkList(res);
                handleDrinkListOnEdit(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm xử lý sự kiện khi ấn nút 'Cập nhật' (trên Modal Edit)
    function onBtnConfirmEditClick() {
        "use strict";
        //Bước 0: Khai báo đối tượng chứa Order Data
        var vObjectRequest = {
            trangThai: "" //3 trang thai open, confirmed, cancel
        };
        //Bước 1: Thu thập dữ liệu 
        getUpdateOrderData(vObjectRequest);
        //Bước 2: Validate dữ liệu Update
        var vIsOrderDataValidate = validateOrderData(vObjectRequest);
        if (vIsOrderDataValidate) {
            //Bước 3: Update Order Data
            getAjaxUpdateOrderData(vObjectRequest);
            //Gọi API lấy danh sách order
            getAllOrder();
            //Bước 4: Xử lý hiện thị front-end
            alert("Cập nhật đơn hàng thành công!");
            loadDataToTable(gDataOrders);
            //Ản modal 
            $("#modal-update").modal("hide");
        }
    }
    //Bước 4 - Hàm API get all order
    function getAllOrder() {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (res) {
                //Lấy response trả về và gán cho biến toàn cục gDataOrders
                gDataOrders = res;
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //Hàm xử lý đổ dữ liệu vào bảng 
    function loadDataToTable(paramResponseObject) {
        "use strict";
        var vUserTable = $("#table-orders").DataTable();
        //Xoá toàn bộ dữ liệu đang có của bảng
        vUserTable.clear();
        //Cập nhật data cho bảng
        vUserTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        vUserTable.draw();

    }
    //Bước 1 - Hàm đọc liệu để Create Order Data
    function getCreateOrderData(paramOrderData) {
        "use strict";
        paramOrderData.kichCo = $("#select-pizza-size").val();
        paramOrderData.duongKinh = $("#select-duong-kinh").val();
        paramOrderData.suon = $("#select-suon-quantity").val();
        paramOrderData.salad = $("#select-salad-size").val();
        paramOrderData.loaiPizza = $("#select-Pizza-Type").val();

        paramOrderData.idVoucher = $("#input-id-voucher").val();
        paramOrderData.idLoaiNuocUong = $("#select-drink-type").val();
        paramOrderData.soLuongNuoc = $("#select-drink-quantity").val();
        paramOrderData.thanhTien = $("#select-price").val();

        paramOrderData.hoTen = $.trim($("#input-name").val());
        paramOrderData.soDienThoai = $.trim($("#input-mobile").val());
        paramOrderData.email = $.trim($("#input-create-email").val());
        paramOrderData.diaChi = $.trim($("#input-address").val());
        paramOrderData.loiNhan = $.trim($("#input-message").val());
        paramOrderData.trangThai = $.trim($("#select-order-status").val());
    }
    //Hàm tạo option cho Select Drink
    function handleDrinkList(paramDrink) {
        "use strict";
        $.each(paramDrink, function (i, item) {
            $("#select-drink-type").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        });
    }
    //Hàm tạo option cho Select Drink (trên Modal Edit)
    function handleDrinkListOnEdit(paramDrink) {
        $.each(paramDrink, function (i, item) {
            $("#select-drink").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        });
    }
    //Hàm tạo option cho Select Pizza Size
    function getPizzaSizeList(paramPizza) {
        "use strict";
        $.each(paramPizza, function (i, item) {
            $("#input-pizzasize").append($('<option>', {
                text: item,
                value: item.toLowerCase()
            }))
        });
    }
    //Hàm tạo option cho Order Status
    function getOrderStatusList(paramOrderStatus) {
        "use strict";
        $.each(paramOrderStatus, function (i, item) {
            $("#input-select-status").append($('<option>', {
                text: item.text,
                value: item.value
            }))
        });
    }

    //Bước 2 - Kiểm tra dữ liệu 
    function validateOrderCreateData(paramOrderData) {
        "use strict";
        if (paramOrderData.kichCo === "NOT_SELECT_SIZE") {
            alert("01 - Bạn vui lòng chọn kích cỡ");
            return false;
        }
        if (paramOrderData.duongKinh === "NOT_SELECT_PIZZA_SIZE") {
            alert("02 -Bạn vui lòng chọn đường kính");
            return false;
        }
        if (paramOrderData.suon === "NOT_SELECT_SUON") {
            alert("03 - Bạn vui lòng chọn sườn");
            return false;
        }
        if (paramOrderData.salad === "NOT_SELECT_SALAD") {
            alert("04 - Bạn vui lòng chọn salad");
            return false;
        }
        if (paramOrderData.loaiPizza === "NOT_SELECT_PIZZA_TYPE") {
            alert("05 - Bạn vui lòng chọn loại pizza");
            return false;
        }
        if (paramOrderData.idLoaiNuocUong === "NOT_SELECT_DRINK_TYPE") {
            alert("06 - Bạn vui lòng chọn loại nước uống");
            return false;
        }
        if (paramOrderData.soLuongNuoc === "NOT_SELECT_DRINK_QUANT") {
            alert("07 - Bạn vui lòng chọn số lượng nước");
            return false;
        }
        if (paramOrderData.thanhTien === "NOT_SELECT_PRICE") {
            alert("08 - Bạn vui lòng chọn giá");
            return false;
        }
        if (paramOrderData.hoTen === "") {
            alert("09 - Bạn vui lòng điền họ và tên");
            return false;
        }
        if (paramOrderData.soDienThoai === "") {
            alert("10 - Bạn vui lòng điền số điện thoại");
            return false;
        }
        if (isNaN(paramOrderData.soDienThoai)) {
            alert("11 - Dữ liệu điện thoại là số");
            return false;
        }
        if (paramOrderData.diaChi === "") {
            alert("12 - Bạn vui lòng điền địa chỉ");
            return false;
        }
        if (paramOrderData.email === "") {
            alert("13 - Bạn vui lòng điền email");
            return false;
        }
        if (paramOrderData.email.includes("@") === false) {
            alert("12 - Email không có @");
            return false;
        }
        return true;
    }
    //Bước 3 - Hàm gọi API insert order
    function postOrderApi(paramOrderData) {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            type: 'POST',
            data: JSON.stringify(paramOrderData),
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                alert('Thêm đơn hàng thành công!');
            },
            error: function (error) {
                alert(error.responseText);
            }
        });
    }
    //Hàm xử lý sự kiện khi nút Edit được ấn
    function onBtnEditOrderClick(paramBtnElement) {
        "use strict";
        console.log("Nút Edit được ấn!");
        var vRowClick = $(paramBtnElement).closest("tr"); //Xác định tr chứa nút bấm được click
        var vTable = $("#table-orders").DataTable(); //Tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); //Lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        console.log("Order Id của đơn hàng tương ứng = " + vDataRow.orderId);
        //Lưu thông tin Id đang được Edit vào biến toàn cục 
        gId = vDataRow.id;
        gOrderId = vDataRow.orderId;
        //Gọi API để truy xuất thông tin và đổ vào modal
        getAjaxOrderDetail(gOrderId);
        //Hiển thị Modal lên
        $("#modal-update").modal("show");
    }
    //Hàm API Detail theo orderId
    function getAjaxOrderDetail(paramOrderId) {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders' + "/" + paramOrderId,
            dataType: 'json',
            type: 'GET',
            async: false,
            success: function (res) {
                handleOrderDetail(res);
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);

            }
        });
    }
    //Hàm đổ dữ liệu vào Modal
    function handleOrderDetail(paramOrderObj) {
        "use strict";
        getDiscount(paramOrderObj.idVourcher);
        var vPrice = paramOrderObj.thanhTien - (paramOrderObj.thanhTien * gDiscountVouchers) / 100;
        $("#input-order-id").val(paramOrderObj.orderId);
        $("#input-order-id").attr('disabled', true);
        $("#input-pizzasize").val(paramOrderObj.kichCo.toLowerCase());
        $("#input-pizzasize").attr('disabled', true);
        $("#input-duongkinh").val(paramOrderObj.duongKinh);
        $("#input-duongkinh").attr('disabled', true);
        $("#input-suon").val(paramOrderObj.suon);
        $("#input-suon").attr('disabled', true);
        $("#input-salad").val(paramOrderObj.salad);
        $("#input-salad").attr('disabled', true);
        $("#input-pizzatype").val(paramOrderObj.loaiPizza);
        $("#input-pizzatype").attr('disabled', true);

        $("#input-idvoucher").val(paramOrderObj.idVourcher);
        $("#input-idvoucher").attr('disabled', true);
        $("#input-giamgia").val(paramOrderObj.giamGia);
        $("#input-giamgia").attr('disabled', true);
        $("#input-thanhtien").val(paramOrderObj.thanhTien);
        $("#input-thanhtien").attr('disabled', true);
        $("#input-price-after-discount").val(vPrice);
        $("#input-price-after-discount").attr('disabled', true);
        $("#select-drink").val(paramOrderObj.idLoaiNuocUong);
        $("#select-drink").attr('disabled', true);
        $("#input-soluongdrink").val(paramOrderObj.soLuongNuoc);
        $("#input-soluongdrink").attr('disabled', true);

        $("#input-fullname").val(paramOrderObj.hoTen);
        $("#input-fullname").attr('disabled', true);
        $("#input-email").val(paramOrderObj.email);
        $("#input-email").attr('disabled', true);
        $("#input-phone").val(paramOrderObj.soDienThoai);
        $("#input-phone").attr('disabled', true);
        $("#input-address-update").val(paramOrderObj.diaChi);
        $("#input-address-update").attr('disabled', true);
        $("#input-message-update").val(paramOrderObj.loiNhan);
        $("#input-message-update").attr('disabled', true);

        $("#input-select-status").val(paramOrderObj.trangThai.toLowerCase());

        $("#input-order-date").val(paramOrderObj.ngayTao);
        $("#input-order-date").attr('disabled', true);
        $("#input-repair-date").val(paramOrderObj.ngayCapNhat);
        $("#input-repair-date").attr('disabled', true);
    }
    //Hàm gọi API xử lý Mã Giảm Giá
    function getDiscount(paramVoucherId) {
        //Bước 1: Read Data 
        "use strict";
        //Bước 2: Kiểm tra dữ liệu input (không có)
        //Bước 3: Gọi Api bằng Ajax
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/' + paramVoucherId,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (res) {
                console.log(res);
                gDiscountVouchers = res.phanTramGiamGia;
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
                console.log("Không tìm thấy voucher");
                alert("Mã voucher không hợp lệ!");
                gDiscountVouchers = 0;
            }
        });
    }
    //Hàm thu thập dữ liệu để update status Order Data
    function getUpdateOrderData(paramObjectRequest) {
        "use strict";
        paramObjectRequest.trangThai = $("#input-select-status").val();
    }
    //Hàm validate dữ liệu để update Order theo orderId (trên Modal Edit)
    function validateOrderData(paramObjectRequest) {
        "use strict";
        if (paramObjectRequest.trangThai === "") {
            alert("Vui lòng cập nhật trạng thái đơn hàng!");
            return false;
        }
        if (paramObjectRequest.trangThai === "Not_Select") {
            alert("Vui lòng cập nhật trạng thái đơn hàng!");
            return false;
        }
        return true;
    }
    //Hàm gọi API Order Data theo Id sau khi ấn nút 'Cập nhật'
    function getAjaxUpdateOrderData(paramObjectRequest) {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders' + '/' + gId,
            type: 'PUT',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(paramObjectRequest),
            async: false,
            success: function (res) {
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm xử lý sự kiện khi ấn nút Delete
    function onBtnDeleteOrderClick(paramBtnElement) {
        "use strict";
        console.log("Nút Delete được ấn!");
        var vRowClick = $(paramBtnElement).closest("tr"); //xác định tr chứa nút bấm được click
        var vTable = $("#table-orders").DataTable(); //tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        console.log("Id tương ứng = " + vDataRow.id);
        //Lưu thông tin studentId đang được edit vào biến toàn cục 
        gId = vDataRow.id;
        //Hiển thị Modal lên
        $("#delete-confirm-modal").modal("show");
    }
    //Hàm xử lý khi ấn nút 'Xác nhận' trên Modal Delete
    function onBtnConfirmDeleteClick() {
        "use strict";
        //Bước 1: Thu thập dữ liệu (không có)
        //Bước 2: Validate dữ liệu (không có)
        //Bước 3: Xoá đơn hàng
        getAjaxDeleteOrder(gId);
        //Gọi API lấy danh sách đơn hàng
        getAllOrder();
        //Bước 4: Xử lý hiển thị front-end
        alert("Xoá đơn hàng thành công!");
        $("#delete-confirm-modal").modal("hide");
        loadDataToTable(gDataOrders);
    }
    //Hàm gọi API sau khi ấn nút 'Xác nhận' trên Modal Thông Báo Xoá
    function getAjaxDeleteOrder(paramId) {
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders' + '/' + paramId,
            type: 'DELETE',
            contentType: 'application/json;charset=UTF-8',
            async: false,
            success: function () {
                console.log("Delete " + paramId + " success!");
            }
        });
    }
});